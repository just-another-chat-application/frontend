# Just Another Chat Application: Frontend

**NB!** This project will depend on [the backend project](https://gitlab.com/just-another-chat-application/backend)

## Prototyping

This is a prototype that will be moved to an Electron application (mainly to avoid domain accessing privatekey).

Early prototyping is done with:

- Socket.io (will be used)
- React (may be used)
- Next (will be replaced)

The prototype will move to MVP development with Electron when message encryption is ready to be developed.

### Features Planned

- Add dummy contact
- Send and receive messages through server
- Mock out UI with grid/flexbox

## MVP

Project will move to MVP development when authentication and message encryption is **needed**.

### Features Planned

- Authentication
- Create user
- Create private/public key
- Message encryption
- Move to Electron
- Settings menu/modal
- Add contacts
